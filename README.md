VizCreations JavaScript library:
================================
A very limited set of classic JavaScript functions and tricks used
in one of our very first projects in webauro. We would like to make it
open-source, so that novice JS programmers can get a hold of
what core (or) functional JavaScript looks like.


How to use it?:
===============
Simply unzip the zipped version to your project folder. Usually
inside a JS folder. Link them up in your web pages, and start
using the functions to your personal wish.


How to install it?:
===================
Read the above section. It's very simple.


Can I change the code?:
=======================
Of course you can. Any open-source project allows you to change
them, and adapt them, wrap them, and sell them too. Well, that's
taking it a bit too far.


API reference:
==============
We are lazy to document our own code. We will do it, but would
need help from some not-too-lazy programmers.


Post a comment at our Facebook page
https://www.facebook.com/pages/VizCreations/196503337046748

Reach our website
http://vizcreations.com

~VIZCREATIONS
