/**
* JavaScript re-usable functions and objects
* author vizcreations, various
* copyright (C) 2015
*/

var wb; // Default webauro object
var wb_oht=0; // Original height of a block
var wb_arr = new Array(); // Always at the helm

wb = {
		doc: document,
		body: document.body,
		html: body,
		elm: body, // The current element the library is confined to
		str: String.prototype,
		reset: function() {
			// RESET all DOM elements
		},
		date: null,
		ck: null,
		timer: null
};

wb.date = new Date();
wb.ck = wb.doc.cookie;

function wb_elm_get($elm) {
	wb.elm=null;
	wb.elm=document.getElementById($elm);
	if(wb.elm)
		return wb.elm; // The current element held by the current context
	else return null;
}

/** Experimental */
/*
* @return mixed
*/
function wb_style_get($elm,$attr) {
	var $prop=null;
	if($elm) {
		wb.elm = $elm;
		$prop=$elm.style.$attr;
	}
	return $prop;
}

function wb_style_set($elm,$attr,$val) {
	if($elm) {
		wb.elm = $elm;
		$elm.style.$attr=$val;
	}
	// nothing to return
}

function wb_toggle_elm($elm,timeout) {
	if($elm) {
		wb.elm = $elm;
		/*if(getStyle($elm,"display")=="none")
			setStyle($elm,"display","block");
		else setStyle($elm,"display","none");*/ // Experimental commented out
			if($elm.style.display=="none")
				if(timeout!=null) 
					wb.timer = setTimeout(function(){$elm.style.display="block";},timeout);
				else
					$elm.style.display="block";
			else
				if(timeout!=null)
					wb.timer = setTimeout(function(){$elm.style.display="none";},timeout);
				else
					$elm.style.display="none";
	}
}

function wb_toggle_height($elm,$ht) {
	if($elm) {
		wb.elm = $elm;
		oht=$elm.clientHeight;
		if(oht>$ht)
			$elm.style.height=oht+"px";
		else $elm.style.height=$ht+"px";
	}
}

function wb_animate(type,msec) {
}

function wb_ck_read(name) { // Lot of grep and parseing..
	var value;
	value = null;
	return value;
}

function wb_ck_write(name,value) {
	var value;
	wb.ck = name+'='+value+'&expires='+wc.date.getFullYear();
}

/**
* Experiment function to know 
* when you hover on the document
*/
function wb_html_hover() {
	// TODO
}

/**
* Experiment function to know
* when you hover on the HTML element
*/
function wb_elm_hover($elm,callback) {
	wb.elm = $elm;
	wb.elm.onmouseover = function() {
		callback(); // Anything you want to do..
	}
}
