/**
 * @abstract Front End JavaScript - AJAX objects for re-use / Webauro lib
 * @author VIZCREATIONS
 * @copyright (C) 2009-2015
 */

/**
 * @license GNU/GPL 3.0
 *
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *
 */

/* JavaScript file for AJAX Object */

/** FLAG for progress */
var wb_ajaxprogress=false; // Use this flag to prevent request pool

/** The global var which can be used repeatedly to save memory */
var wb_ajax = null; // Remember to make it null before request

/** Taken from w3schools.com */
function wb_ajax_get() {
	var ajax;
	try {
		ajax = new XMLHttpRequest(); // Most new browsers use this
	} catch(e) {
		try {
			ajax = new ActiveXObject("Msxml2.XMLHTTP"); // MSIE 5.0
		} catch(e) {
			try {
				ajax = new ActiveXObject("Microsoft.XMLHTTP"); // MSIE 6.0
			} catch(e) {
				ajax = null; // Use your own message for achieving null on request
			}
		}
	}
	return ajax;
	
}

/**
* Useful function to reuse
* Substitute the server url in $req_uri and the HTML JS object in $htmldiv
* @return null
*/
function wb_ajax_req(req_uri, http, htmldiv) {
	wb_ajax = null;
	wb_ajaxprogress = true;
	wb_ajax = wb_ajax_get();
	if(wb_ajax == null) {
		window.alert("Webauro warning: AJAX request failed!");
		console.log("Webauro warning: AJAX request failed!");
		wb_ajaxprogress = false;
		return false;
	}

	wb_ajax.open(http, req_uri, true);
	wb_ajax.send(null);

	/** The loop */
	wb_ajax.onreadystatechange = wb_ajax_fnc(function() {
		wb_ajaxprogress = false;
		htmldiv.innerHTML = wb_ajax.responseText; // Use [responseXML] if you're expecting XML
	});
}

// The background function that takes care of processing..
function wb_ajax_fnc(callback) {
	if(wb_ajax.readyState == 4 
		|| wb_ajax.readyState == 'complete'
	) {
		if(wb_ajax.status == 200) {
			callback();
		}
	}
}
